﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Formularz przesłania metodę dispose, aby wyczyścić listę składników.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wymagane przez Projektanta formularzy systemu Windows
    Private components As System.ComponentModel.IContainer

    'UWAGA: następująca procedura jest wymagana przez Projektanta formularzy systemu Windows
    'Możesz to modyfikować, używając Projektanta formularzy systemu Windows. 
    'Nie należy modyfikować za pomocą edytora kodu.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button200 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button49 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button47 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button46 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button45 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button44 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button43 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button42 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button41 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button40 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button39 = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button38 = New System.Windows.Forms.Button()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button37 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Button36 = New System.Windows.Forms.Button()
        Me.Button48 = New System.Windows.Forms.Button()
        Me.Button35 = New System.Windows.Forms.Button()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.Button34 = New System.Windows.Forms.Button()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.Button33 = New System.Windows.Forms.Button()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.Button32 = New System.Windows.Forms.Button()
        Me.Button17 = New System.Windows.Forms.Button()
        Me.Button31 = New System.Windows.Forms.Button()
        Me.Button18 = New System.Windows.Forms.Button()
        Me.Button30 = New System.Windows.Forms.Button()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.Button29 = New System.Windows.Forms.Button()
        Me.Button20 = New System.Windows.Forms.Button()
        Me.Button28 = New System.Windows.Forms.Button()
        Me.Button21 = New System.Windows.Forms.Button()
        Me.Button27 = New System.Windows.Forms.Button()
        Me.Button22 = New System.Windows.Forms.Button()
        Me.Button26 = New System.Windows.Forms.Button()
        Me.Button23 = New System.Windows.Forms.Button()
        Me.Button25 = New System.Windows.Forms.Button()
        Me.Button24 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button53 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 256)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(85, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Your ID key:"
        '
        'Button200
        '
        Me.Button200.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button200.Location = New System.Drawing.Point(897, 473)
        Me.Button200.Name = "Button200"
        Me.Button200.Size = New System.Drawing.Size(112, 45)
        Me.Button200.TabIndex = 2
        Me.Button200.Text = "Unlock"
        Me.Button200.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(894, 453)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 17)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Label2"
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(60, 31)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(900, 51)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = resources.GetString("Label3.Text")
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Button10)
        Me.GroupBox1.Controls.Add(Me.Button49)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.Button47)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Button46)
        Me.GroupBox1.Controls.Add(Me.Button3)
        Me.GroupBox1.Controls.Add(Me.Button45)
        Me.GroupBox1.Controls.Add(Me.Button4)
        Me.GroupBox1.Controls.Add(Me.Button44)
        Me.GroupBox1.Controls.Add(Me.Button5)
        Me.GroupBox1.Controls.Add(Me.Button43)
        Me.GroupBox1.Controls.Add(Me.Button6)
        Me.GroupBox1.Controls.Add(Me.Button42)
        Me.GroupBox1.Controls.Add(Me.Button7)
        Me.GroupBox1.Controls.Add(Me.Button41)
        Me.GroupBox1.Controls.Add(Me.Button8)
        Me.GroupBox1.Controls.Add(Me.Button40)
        Me.GroupBox1.Controls.Add(Me.Button9)
        Me.GroupBox1.Controls.Add(Me.Button39)
        Me.GroupBox1.Controls.Add(Me.Button11)
        Me.GroupBox1.Controls.Add(Me.Button38)
        Me.GroupBox1.Controls.Add(Me.Button12)
        Me.GroupBox1.Controls.Add(Me.Button37)
        Me.GroupBox1.Controls.Add(Me.Button13)
        Me.GroupBox1.Controls.Add(Me.Button36)
        Me.GroupBox1.Controls.Add(Me.Button48)
        Me.GroupBox1.Controls.Add(Me.Button35)
        Me.GroupBox1.Controls.Add(Me.Button14)
        Me.GroupBox1.Controls.Add(Me.Button34)
        Me.GroupBox1.Controls.Add(Me.Button15)
        Me.GroupBox1.Controls.Add(Me.Button33)
        Me.GroupBox1.Controls.Add(Me.Button16)
        Me.GroupBox1.Controls.Add(Me.Button32)
        Me.GroupBox1.Controls.Add(Me.Button17)
        Me.GroupBox1.Controls.Add(Me.Button31)
        Me.GroupBox1.Controls.Add(Me.Button18)
        Me.GroupBox1.Controls.Add(Me.Button30)
        Me.GroupBox1.Controls.Add(Me.Button19)
        Me.GroupBox1.Controls.Add(Me.Button29)
        Me.GroupBox1.Controls.Add(Me.Button20)
        Me.GroupBox1.Controls.Add(Me.Button28)
        Me.GroupBox1.Controls.Add(Me.Button21)
        Me.GroupBox1.Controls.Add(Me.Button27)
        Me.GroupBox1.Controls.Add(Me.Button22)
        Me.GroupBox1.Controls.Add(Me.Button26)
        Me.GroupBox1.Controls.Add(Me.Button23)
        Me.GroupBox1.Controls.Add(Me.Button25)
        Me.GroupBox1.Controls.Add(Me.Button24)
        Me.GroupBox1.Location = New System.Drawing.Point(1, 276)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(500, 198)
        Me.GroupBox1.TabIndex = 115
        Me.GroupBox1.TabStop = False
        '
        'Button10
        '
        Me.Button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button10.Location = New System.Drawing.Point(299, 21)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(36, 30)
        Me.Button10.TabIndex = 74
        Me.Button10.Text = "Button10"
        Me.Button10.UseVisualStyleBackColor = True
        '
        'Button49
        '
        Me.Button49.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button49.Location = New System.Drawing.Point(39, 165)
        Me.Button49.Name = "Button49"
        Me.Button49.Size = New System.Drawing.Size(296, 30)
        Me.Button49.TabIndex = 113
        Me.Button49.Text = "Space"
        Me.Button49.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Location = New System.Drawing.Point(8, 21)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(36, 30)
        Me.Button1.TabIndex = 65
        Me.Button1.Text = "1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button47
        '
        Me.Button47.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button47.Location = New System.Drawing.Point(299, 129)
        Me.Button47.Name = "Button47"
        Me.Button47.Size = New System.Drawing.Size(33, 30)
        Me.Button47.TabIndex = 112
        Me.Button47.Text = "1"
        Me.Button47.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Location = New System.Drawing.Point(40, 21)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(36, 30)
        Me.Button2.TabIndex = 66
        Me.Button2.Text = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button46
        '
        Me.Button46.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button46.Location = New System.Drawing.Point(268, 129)
        Me.Button46.Name = "Button46"
        Me.Button46.Size = New System.Drawing.Size(36, 30)
        Me.Button46.TabIndex = 111
        Me.Button46.Text = "1"
        Me.Button46.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Location = New System.Drawing.Point(73, 21)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(36, 30)
        Me.Button3.TabIndex = 67
        Me.Button3.Text = "Button3"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button45
        '
        Me.Button45.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button45.Location = New System.Drawing.Point(236, 129)
        Me.Button45.Name = "Button45"
        Me.Button45.Size = New System.Drawing.Size(36, 30)
        Me.Button45.TabIndex = 110
        Me.Button45.Text = "1"
        Me.Button45.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Location = New System.Drawing.Point(106, 21)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(36, 30)
        Me.Button4.TabIndex = 68
        Me.Button4.Text = "Button4"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button44
        '
        Me.Button44.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button44.Location = New System.Drawing.Point(204, 129)
        Me.Button44.Name = "Button44"
        Me.Button44.Size = New System.Drawing.Size(36, 30)
        Me.Button44.TabIndex = 109
        Me.Button44.Text = "1"
        Me.Button44.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Location = New System.Drawing.Point(138, 21)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(36, 30)
        Me.Button5.TabIndex = 69
        Me.Button5.Text = "Button5"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button43
        '
        Me.Button43.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button43.Location = New System.Drawing.Point(171, 129)
        Me.Button43.Name = "Button43"
        Me.Button43.Size = New System.Drawing.Size(36, 30)
        Me.Button43.TabIndex = 108
        Me.Button43.Text = "1"
        Me.Button43.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Location = New System.Drawing.Point(171, 21)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(36, 30)
        Me.Button6.TabIndex = 70
        Me.Button6.Text = "Button6"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button42
        '
        Me.Button42.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button42.Location = New System.Drawing.Point(138, 129)
        Me.Button42.Name = "Button42"
        Me.Button42.Size = New System.Drawing.Size(36, 30)
        Me.Button42.TabIndex = 107
        Me.Button42.Text = "1"
        Me.Button42.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Location = New System.Drawing.Point(204, 21)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(36, 30)
        Me.Button7.TabIndex = 71
        Me.Button7.Text = "Button7"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button41
        '
        Me.Button41.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button41.Location = New System.Drawing.Point(106, 129)
        Me.Button41.Name = "Button41"
        Me.Button41.Size = New System.Drawing.Size(36, 30)
        Me.Button41.TabIndex = 106
        Me.Button41.Text = "1"
        Me.Button41.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Location = New System.Drawing.Point(236, 21)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(36, 30)
        Me.Button8.TabIndex = 72
        Me.Button8.Text = "Button8"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Button40
        '
        Me.Button40.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button40.Location = New System.Drawing.Point(73, 129)
        Me.Button40.Name = "Button40"
        Me.Button40.Size = New System.Drawing.Size(36, 30)
        Me.Button40.TabIndex = 105
        Me.Button40.Text = "1"
        Me.Button40.UseVisualStyleBackColor = True
        '
        'Button9
        '
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.Location = New System.Drawing.Point(268, 21)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(36, 30)
        Me.Button9.TabIndex = 73
        Me.Button9.Text = "Button9"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Button39
        '
        Me.Button39.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button39.Location = New System.Drawing.Point(39, 129)
        Me.Button39.Name = "Button39"
        Me.Button39.Size = New System.Drawing.Size(36, 30)
        Me.Button39.TabIndex = 104
        Me.Button39.Text = "1"
        Me.Button39.UseVisualStyleBackColor = True
        '
        'Button11
        '
        Me.Button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button11.Location = New System.Drawing.Point(331, 21)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(36, 30)
        Me.Button11.TabIndex = 75
        Me.Button11.Text = "Button11"
        Me.Button11.UseVisualStyleBackColor = True
        '
        'Button38
        '
        Me.Button38.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button38.Location = New System.Drawing.Point(8, 129)
        Me.Button38.Name = "Button38"
        Me.Button38.Size = New System.Drawing.Size(36, 30)
        Me.Button38.TabIndex = 103
        Me.Button38.Text = "1"
        Me.Button38.UseVisualStyleBackColor = True
        '
        'Button12
        '
        Me.Button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button12.Location = New System.Drawing.Point(362, 21)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(36, 30)
        Me.Button12.TabIndex = 76
        Me.Button12.Text = "Button12"
        Me.Button12.UseVisualStyleBackColor = True
        '
        'Button37
        '
        Me.Button37.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button37.Location = New System.Drawing.Point(331, 93)
        Me.Button37.Name = "Button37"
        Me.Button37.Size = New System.Drawing.Size(33, 30)
        Me.Button37.TabIndex = 102
        Me.Button37.Text = "1"
        Me.Button37.UseVisualStyleBackColor = True
        '
        'Button13
        '
        Me.Button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button13.Location = New System.Drawing.Point(393, 21)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(95, 30)
        Me.Button13.TabIndex = 77
        Me.Button13.Text = "Button13"
        Me.Button13.UseVisualStyleBackColor = True
        '
        'Button36
        '
        Me.Button36.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button36.Location = New System.Drawing.Point(299, 93)
        Me.Button36.Name = "Button36"
        Me.Button36.Size = New System.Drawing.Size(36, 30)
        Me.Button36.TabIndex = 101
        Me.Button36.Text = "1"
        Me.Button36.UseVisualStyleBackColor = True
        '
        'Button48
        '
        Me.Button48.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button48.Location = New System.Drawing.Point(8, 165)
        Me.Button48.Name = "Button48"
        Me.Button48.Size = New System.Drawing.Size(36, 30)
        Me.Button48.TabIndex = 78
        Me.Button48.Text = "◇"
        Me.Button48.UseVisualStyleBackColor = True
        '
        'Button35
        '
        Me.Button35.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button35.Location = New System.Drawing.Point(268, 93)
        Me.Button35.Name = "Button35"
        Me.Button35.Size = New System.Drawing.Size(36, 30)
        Me.Button35.TabIndex = 100
        Me.Button35.Text = "1"
        Me.Button35.UseVisualStyleBackColor = True
        '
        'Button14
        '
        Me.Button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button14.Location = New System.Drawing.Point(8, 57)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(36, 30)
        Me.Button14.TabIndex = 79
        Me.Button14.Text = "1"
        Me.Button14.UseVisualStyleBackColor = True
        '
        'Button34
        '
        Me.Button34.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button34.Location = New System.Drawing.Point(236, 93)
        Me.Button34.Name = "Button34"
        Me.Button34.Size = New System.Drawing.Size(36, 30)
        Me.Button34.TabIndex = 99
        Me.Button34.Text = "1"
        Me.Button34.UseVisualStyleBackColor = True
        '
        'Button15
        '
        Me.Button15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button15.Location = New System.Drawing.Point(40, 57)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(36, 30)
        Me.Button15.TabIndex = 80
        Me.Button15.Text = "1"
        Me.Button15.UseVisualStyleBackColor = True
        '
        'Button33
        '
        Me.Button33.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button33.Location = New System.Drawing.Point(204, 93)
        Me.Button33.Name = "Button33"
        Me.Button33.Size = New System.Drawing.Size(36, 30)
        Me.Button33.TabIndex = 98
        Me.Button33.Text = "1"
        Me.Button33.UseVisualStyleBackColor = True
        '
        'Button16
        '
        Me.Button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button16.Location = New System.Drawing.Point(73, 57)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(36, 30)
        Me.Button16.TabIndex = 81
        Me.Button16.Text = "1"
        Me.Button16.UseVisualStyleBackColor = True
        '
        'Button32
        '
        Me.Button32.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button32.Location = New System.Drawing.Point(171, 93)
        Me.Button32.Name = "Button32"
        Me.Button32.Size = New System.Drawing.Size(36, 30)
        Me.Button32.TabIndex = 97
        Me.Button32.Text = "1"
        Me.Button32.UseVisualStyleBackColor = True
        '
        'Button17
        '
        Me.Button17.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button17.Location = New System.Drawing.Point(106, 57)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(36, 30)
        Me.Button17.TabIndex = 82
        Me.Button17.Text = "1"
        Me.Button17.UseVisualStyleBackColor = True
        '
        'Button31
        '
        Me.Button31.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button31.Location = New System.Drawing.Point(138, 93)
        Me.Button31.Name = "Button31"
        Me.Button31.Size = New System.Drawing.Size(36, 30)
        Me.Button31.TabIndex = 96
        Me.Button31.Text = "1"
        Me.Button31.UseVisualStyleBackColor = True
        '
        'Button18
        '
        Me.Button18.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button18.Location = New System.Drawing.Point(138, 57)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(36, 30)
        Me.Button18.TabIndex = 83
        Me.Button18.Text = "1"
        Me.Button18.UseVisualStyleBackColor = True
        '
        'Button30
        '
        Me.Button30.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button30.Location = New System.Drawing.Point(106, 93)
        Me.Button30.Name = "Button30"
        Me.Button30.Size = New System.Drawing.Size(36, 30)
        Me.Button30.TabIndex = 95
        Me.Button30.Text = "1"
        Me.Button30.UseVisualStyleBackColor = True
        '
        'Button19
        '
        Me.Button19.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button19.Location = New System.Drawing.Point(171, 57)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(36, 30)
        Me.Button19.TabIndex = 84
        Me.Button19.Text = "1"
        Me.Button19.UseVisualStyleBackColor = True
        '
        'Button29
        '
        Me.Button29.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button29.Location = New System.Drawing.Point(73, 93)
        Me.Button29.Name = "Button29"
        Me.Button29.Size = New System.Drawing.Size(36, 30)
        Me.Button29.TabIndex = 94
        Me.Button29.Text = "1"
        Me.Button29.UseVisualStyleBackColor = True
        '
        'Button20
        '
        Me.Button20.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button20.Location = New System.Drawing.Point(204, 57)
        Me.Button20.Name = "Button20"
        Me.Button20.Size = New System.Drawing.Size(36, 30)
        Me.Button20.TabIndex = 85
        Me.Button20.Text = "1"
        Me.Button20.UseVisualStyleBackColor = True
        '
        'Button28
        '
        Me.Button28.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button28.Location = New System.Drawing.Point(40, 93)
        Me.Button28.Name = "Button28"
        Me.Button28.Size = New System.Drawing.Size(36, 30)
        Me.Button28.TabIndex = 93
        Me.Button28.Text = "1"
        Me.Button28.UseVisualStyleBackColor = True
        '
        'Button21
        '
        Me.Button21.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button21.Location = New System.Drawing.Point(236, 57)
        Me.Button21.Name = "Button21"
        Me.Button21.Size = New System.Drawing.Size(36, 30)
        Me.Button21.TabIndex = 86
        Me.Button21.Text = "1"
        Me.Button21.UseVisualStyleBackColor = True
        '
        'Button27
        '
        Me.Button27.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button27.Location = New System.Drawing.Point(8, 93)
        Me.Button27.Name = "Button27"
        Me.Button27.Size = New System.Drawing.Size(36, 30)
        Me.Button27.TabIndex = 92
        Me.Button27.Text = "1"
        Me.Button27.UseVisualStyleBackColor = True
        '
        'Button22
        '
        Me.Button22.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button22.Location = New System.Drawing.Point(268, 57)
        Me.Button22.Name = "Button22"
        Me.Button22.Size = New System.Drawing.Size(36, 30)
        Me.Button22.TabIndex = 87
        Me.Button22.Text = "1"
        Me.Button22.UseVisualStyleBackColor = True
        '
        'Button26
        '
        Me.Button26.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button26.Location = New System.Drawing.Point(393, 57)
        Me.Button26.Name = "Button26"
        Me.Button26.Size = New System.Drawing.Size(36, 30)
        Me.Button26.TabIndex = 91
        Me.Button26.Text = "1"
        Me.Button26.UseVisualStyleBackColor = True
        '
        'Button23
        '
        Me.Button23.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button23.Location = New System.Drawing.Point(299, 57)
        Me.Button23.Name = "Button23"
        Me.Button23.Size = New System.Drawing.Size(36, 30)
        Me.Button23.TabIndex = 88
        Me.Button23.Text = "1"
        Me.Button23.UseVisualStyleBackColor = True
        '
        'Button25
        '
        Me.Button25.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button25.Location = New System.Drawing.Point(362, 57)
        Me.Button25.Name = "Button25"
        Me.Button25.Size = New System.Drawing.Size(36, 30)
        Me.Button25.TabIndex = 90
        Me.Button25.Text = "1"
        Me.Button25.UseVisualStyleBackColor = True
        '
        'Button24
        '
        Me.Button24.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button24.Location = New System.Drawing.Point(331, 57)
        Me.Button24.Name = "Button24"
        Me.Button24.Size = New System.Drawing.Size(36, 30)
        Me.Button24.TabIndex = 89
        Me.Button24.Text = "1"
        Me.Button24.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.SystemColors.HighlightText
        Me.TextBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TextBox1.Cursor = System.Windows.Forms.Cursors.No
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(0, 473)
        Me.TextBox1.Multiline = True
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(899, 45)
        Me.TextBox1.TabIndex = 3
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.TextBox1.WordWrap = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Button53)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.TextBox1)
        Me.GroupBox2.Controls.Add(Me.Button200)
        Me.GroupBox2.Controls.Add(Me.GroupBox1)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1009, 518)
        Me.GroupBox2.TabIndex = 119
        Me.GroupBox2.TabStop = False
        '
        'Button53
        '
        Me.Button53.BackColor = System.Drawing.Color.Red
        Me.Button53.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button53.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.Button53.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Button53.Location = New System.Drawing.Point(913, 7)
        Me.Button53.Name = "Button53"
        Me.Button53.Size = New System.Drawing.Size(96, 41)
        Me.Button53.TabIndex = 119
        Me.Button53.Text = "EXIT"
        Me.Button53.UseVisualStyleBackColor = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.ClientSize = New System.Drawing.Size(1033, 542)
        Me.Controls.Add(Me.GroupBox2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultBounds
        Me.Text = "Form1"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Button200 As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Timer1 As Timer
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Button10 As Button
    Friend WithEvents Button49 As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Button47 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Button46 As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents Button45 As Button
    Friend WithEvents Button4 As Button
    Friend WithEvents Button44 As Button
    Friend WithEvents Button5 As Button
    Friend WithEvents Button43 As Button
    Friend WithEvents Button6 As Button
    Friend WithEvents Button42 As Button
    Friend WithEvents Button7 As Button
    Friend WithEvents Button41 As Button
    Friend WithEvents Button8 As Button
    Friend WithEvents Button40 As Button
    Friend WithEvents Button9 As Button
    Friend WithEvents Button39 As Button
    Friend WithEvents Button11 As Button
    Friend WithEvents Button38 As Button
    Friend WithEvents Button12 As Button
    Friend WithEvents Button37 As Button
    Friend WithEvents Button13 As Button
    Friend WithEvents Button36 As Button
    Friend WithEvents Button48 As Button
    Friend WithEvents Button35 As Button
    Friend WithEvents Button14 As Button
    Friend WithEvents Button34 As Button
    Friend WithEvents Button15 As Button
    Friend WithEvents Button33 As Button
    Friend WithEvents Button16 As Button
    Friend WithEvents Button32 As Button
    Friend WithEvents Button17 As Button
    Friend WithEvents Button31 As Button
    Friend WithEvents Button18 As Button
    Friend WithEvents Button30 As Button
    Friend WithEvents Button19 As Button
    Friend WithEvents Button29 As Button
    Friend WithEvents Button20 As Button
    Friend WithEvents Button28 As Button
    Friend WithEvents Button21 As Button
    Friend WithEvents Button27 As Button
    Friend WithEvents Button22 As Button
    Friend WithEvents Button26 As Button
    Friend WithEvents Button23 As Button
    Friend WithEvents Button25 As Button
    Friend WithEvents Button24 As Button
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Button53 As Button
End Class
